# nine-letter-puzzle-solver

![](https://img.shields.io/badge/written%20in-PHP-blue)

Solver for the nine-letter puzzle.

The distribution includes a bundled copy of Debian 8 "Jessie"'s `american-english` dictionary.

## See Also

- Puzzle description on http://www.snopes.com/language/puzzlers/9letters.asp


## Download

- [⬇️ nine-letter-puzzle-solver-v1.zip](dist-archive/nine-letter-puzzle-solver-v1.zip) *(213.43 KiB)*
